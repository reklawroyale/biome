/*
    Biome

    Check Home.md for documentation.
*/

    import java.security.SecureRandom;
    import java.util.Scanner;
    import java.util.ArrayList;
    import java.lang.reflect.*;

    public class Biome {

        public static void main(String[] args){

            final int TERRITORY = 10;

            Animal[] animals = new Animal[TERRITORY];

            SecureRandom random = new SecureRandom();

            int COYOTE_SPAWN_PROBABILITY = 30;
            int COYOTE_MOVE_PROBABILITY = 50;
            int COYOTE_MAX_AGE = 20;

            int RABBIT_SPAWN_PROBABILITY = 50;
            int RABBIT_MOVE_PROBABILITY = 20;          
            int RABBIT_MAX_AGE = 15;

            ArrayList<Integer> emptyTerritory = new ArrayList<Integer>();

            //territory initialization
            for (int i = 0; i < TERRITORY; i++){
                //generate an integer on [0,99]
                int number = random.nextInt(100);
                if (number < RABBIT_SPAWN_PROBABILITY){
                    //place a new rabbit in this index
                    //Rabbit r = new Rabbit();         //herbivores disabled
                    //animals[i] = r;
                }
                else if (number < (RABBIT_SPAWN_PROBABILITY + COYOTE_SPAWN_PROBABILITY)){
                    //place a new coyote in this index
                    Coyote c = new Coyote();
                    animals[i] = c;
                }
                else animals[i] = null;
            }

            boolean running = true;

            Scanner input = new Scanner(System.in);

            //output initial territory
            outputBiome(animals,TERRITORY);

            String s = input.nextLine();
            if (s.equals("q")) running = false;

            while (running){
                //Each time step:

                //based on RNG each animal either attempts to move into an adjacent array cell or stay where it is.

                /*Plan:
                    iterate through all territory spaces
                    check if animal moves
                    the animal can only move if its "just moved" field is false. if true, set to false
                        if so, check for boundaries
                            if the new valid spot is empty, perform simple move
                        if the move space has an animal of the same species, move neither. Consult the list of empty spaces. Choose a random one and place there a new animal of this species
                        if the animals don't share species, delete the prey species and add a kill to the predator species
                */

                //TODO: foreach Animal in animals
                //foreach (Animal i : animals){
                for (int i = 0; i < TERRITORY; i++){

                    if (animals[i] == null){
                        //do nothing
                    }
                    else { 
                        Class cls = animals[i].getClass();
                        String name = cls.getSimpleName();
                        if (name.equals("Rabbit")){
                            //Strategy:

                            //determine if it attempts to move
                            //check if each adjacent move would be valid
                                //if not, move to valid space
                                //if so, choose a space randomly and move there
                                    //if space not occupied, move rabbit
                                    //if space occupied by rabbit
                                        //perform new rabbit generation
                                    //if space occupied by coyote
                                        //set rabbit's index in animals[] to null
                                        //increase this coyote's kill counter

                            int willMove = random.nextInt(100);
                            if (willMove < RABBIT_MOVE_PROBABILITY){
                                //check if each adjacent move would be valid
                                int attemptMove = moveValidator(i, TERRITORY);
                                if (attemptMove == 0){
                                    //if all valid, choose one randomly
                                    int randomMove = random.nextInt(2);
                                    attemptMove = (randomMove == 0) ? -1 : 1;
                                }
                                if (attemptMove == -1){
                                    attemptMove = i-1;
                                }
                                else if (attemptMove == 1){
                                    attemptMove = i+1;
                                }
                                        
                                //if space not occupied, move rabbit
                                if (animals[attemptMove] == null){
                                    //increase rabbit's age; check for age death
                                    animals[i].survive();
                                    if (animals[i].getAge() < RABBIT_MAX_AGE){
                                        animals[attemptMove] = animals[i];
                                    }
                                    animals[i] = null;
                                    emptyTerritory.add(i);
                                }
                                else {
                                    cls = animals[attemptMove].getClass();
                                    name = cls.getSimpleName();
                                    //if space occupied by rabbit, perform new rabbit generation
                                    if (name.equals("Rabbit")){
                                        //choose a random empty spot and create a new rabbit there, if there's room in the territory
                                        int size = emptyTerritory.size();
                                        if(size < TERRITORY && size > 0){
                                            int choice = random.nextInt(size);
                                            //remove the spot from the empty list
                                            int newChoice = emptyTerritory.remove(choice);
                                            animals[newChoice] = new Rabbit();
                                        }
                                        //increase the original rabbit's age; check for age death
                                        animals[i].survive();
                                        if (animals[i].getAge() > RABBIT_MAX_AGE){
                                            animals[i] = null;
                                            emptyTerritory.add(i);
                                        }
                                    }                                    
                                    else if (name.equals("Coyote")){
                                        //if space occupied by coyote, set animals[i] to null, update empty list, and increase appropriate coyote's kill counter
                                        animals[i] = null;
                                        emptyTerritory.add(i);
                                        Coyote c = (Coyote)animals[attemptMove];
                                        c.eatPrey();
                                    }
                                }
                                    
                            }
                            else{
                                //increase rabbit's age
                                animals[i].survive();
                                //if it's over a certain age, set animals[i] to null
                                if (animals[i].getAge() > RABBIT_MAX_AGE){
                                    animals[i] = null;
                                    emptyTerritory.add(i);
                                }
                            }
                        }
                        else if (name.equals("Coyote")){

                            //Coyote Strategy:

                            //determine if it attempts to move
                            //check if each adjacent move would be valid
                            //if not, move to valid space
                            //if so, choose a space randomly and move there
                                    //if space not occupied, move coyote
                                    //if space occupied by rabbit
                                            //set rabbit's index in animals[] to null
                                            //increase this coyote's kill counter
                                            //move the coyote
                                    //if space occupied by coyote
                                            //perform new coyote generation

                            int willMove = random.nextInt(100);
                            if (willMove < COYOTE_MOVE_PROBABILITY){

                                int attemptMove = moveValidator(i, TERRITORY);
                                if (attemptMove == 0){
                                    //if all valid, choose one randomly
                                    int randomMove = random.nextInt(2);
                                    attemptMove = (randomMove == 0) ? -1 : 1;
                                }
                                if (attemptMove == -1){
                                    attemptMove = i-1;
                                }
                                else if (attemptMove == 1){
                                    attemptMove = i+1;
                                }
                                        
                                //if space not occupied, move coyote
                                if (animals[attemptMove] == null){
                                    //increase coyote's age; check for age death
                                    animals[i].survive();
                                    if (animals[i].getAge() < COYOTE_MAX_AGE){
                                        animals[attemptMove] = animals[i];
                                    }
                                    animals[i] = null;
                                    emptyTerritory.add(i);
                                }
                                else {
                                    cls = animals[attemptMove].getClass();
                                    name = cls.getSimpleName();
                                    //if space occupied by coyote, perform new coyote generation
                                    if (name.equals("Coyote")){
                                        //choose a random empty spot and create a new coyote there
                                        int size = emptyTerritory.size();
                                        if(size < TERRITORY && size > 0){
                                            int choice = random.nextInt(size);
                                            //remove the spot from the empty list
                                            int newChoice = emptyTerritory.remove(choice);
                                            animals[newChoice] = new Coyote();
                                        }
                                        //increase the original coyote's age; check for age death
                                        animals[i].survive();
                                        if (animals[i].getAge() > COYOTE_MAX_AGE){
                                            animals[i] = null;
                                            emptyTerritory.add(i);
                                        }
                                    }                                    
                                    else if (name.equals("Rabbit")){
                                        //if space occupied by rabbit
                                                
                                            //update empty list
                                            //increase coyote's kill counter
                                            //increase coyote's age
                                            //check for age death
                                            //set animals[i] to null

                                    animals[i].survive();
                                    if (animals[i].getAge() < COYOTE_MAX_AGE){
                                        animals[attemptMove] = animals[i];
                                    }
                                    animals[i] = null;
                                    emptyTerritory.add(i);
                                    animals[i] = null;
                                    emptyTerritory.add(i);
                                    Coyote c = (Coyote)animals[attemptMove];
                                    c.eatPrey();
                                    }
                                }
                            }
                            else {
                                //increase coyote's age
                                animals[i].survive();
                                //if it's over a certain age, set animals[i] to null
                                if (animals[i].getAge() > COYOTE_MAX_AGE){
                                    animals[i] = null;
                                    emptyTerritory.add(i);
                                }
                            }
                        }
                    }              
                }

                //output the current state of the biome            
                outputBiome(animals,TERRITORY);
                System.out.printf("\nEmpty spaces: %d", emptyTerritory.size() );
                System.out.printf("\nEmpty list: %s", emptyTerritory.toString() );

                s = input.nextLine();
                if (s.equals("q")) running = false;

            }

            //output final stats??
        }

        static void outputBiome(Animal[] animals, int TERRITORY){
            System.out.println("");
            //System.out.printf("\nLegend:\n  Empty:  | |\n  Rabbit: |r|\n  Coyote: |c|\n\n");
            for (int i = 0; i < TERRITORY; i++){
                if (animals[i] == null){
                    System.out.printf("| |");
                }
                else{
                    Class cls = animals[i].getClass();
                    String name = cls.getSimpleName();
                    if (name.equals("Rabbit")){
                        System.out.printf("|r|");
                    }
                    if (name.equals("Coyote")){
                        System.out.printf("|c|");
                    }
                }                    
            }
            //System.out.printf("\n\n'q' to quit ");
        }

        static int moveValidator(int i, int TERRITORY){
            //check if each adjacent move would be valid
            //if not, move to valid space
            if (i == 0){
                //on bottommost boundary, so move one cell up
                return 1;
            }
            else if (i == TERRITORY - 1){
                //on topmost boundary, so move one cell down
                return -1;
            }
            //if so, return to calling function so it can choose randomly
            else{
                return 0;
            }
        }
    }

    abstract class Animal {
        int age;

        Animal(){
            age = 0;
        }

        public void survive(){
            age++;
        }

        public int getAge(){
            return age;
        }
    }

    class Coyote extends Animal {
        int prey_eaten;

        Coyote(){
            super();
            prey_eaten = 0;
        }

        public void eatPrey(){
            prey_eaten++;
        }
    }

    class Rabbit extends Animal {

        Rabbit(){
            super();
        }
    }
