# Biome

This is a project I started to explore data structures in Java. I plan to use it to further explore topics like object-oriented programming and intelligence in simulations.

[Follow me on Twitter for updates!](https://twitter.com/ReklawCodes)

[Join my Discord for discussion!](https://discord.gg/6A97Ecg)

[Subscribe to me on Youtube!](https://www.youtube.com/channel/UCPtkVhB0kw_KkhaXIJzthYg)

[Follow me on Twitch!](https://www.twitch.tv/realReklaw)

## Project goal: Write a program that models a biome.
 - Represent the biome as an array. 
 - Each cell contains an Animal object, or is set to null.
 - Every time step, animals may attempt to move into another space.
 - Create behaviors to model animal lifecycles including predation and mating.
 - Instantiate Animal objects and take advantage of OOP to improve the model.

## TODO
 - Introduce Organism superclass, Plants etc.
 - Move objects to their own files; consider package structure
 - Iterate through Organisms instead of/in addition to territory spaces. This should solve the "already moved" issue
 - Fix EmptyList bug; the list shouldn't be necessary with a better data structure
 - Auto repopulate when the biome goes extinct
